﻿using System;
using System.Collections.Generic;
using System.Linq;
using NUnit.Framework;

namespace PolygonIntersections
{
	class Poligon
	{
		public PointD[] Vertexes { get; private set; }

		public Poligon(PointD[] vertexes)
		{
			Vertexes = vertexes;
		}
		public IEnumerable<Segment> GetContour()
		{
			for (int i = 1; i < Vertexes.Length; i++)
			{
				yield return new Segment(Vertexes[i-1], Vertexes[i]);
			}
			yield return new Segment(Vertexes.Last(), Vertexes.First());
		}
		public static IEnumerable<Segment> GetAllSegments(params Poligon[] poligons)
		{
			return poligons.SelectMany(p => p.GetContour());
		}
	}

	[TestFixture]
	internal class PoligonTester
	{
		[Test]
		public void GetContourTest()
		{
			var p = new Poligon(new PointD[]
			                    	{
			                    		new PointD(0, 0),
			                    		new PointD(2, 2),
			                    		new PointD(6, 1),
			                    		new PointD(4, 0),
			                    	});
			foreach (var segment in p.GetContour())
			{
				Console.Out.WriteLine(segment);
			}
		} 
	}
}
