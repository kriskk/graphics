﻿using System;
using NUnit.Framework;

namespace PolygonIntersections
{
	public class PointD
	{
		public double X { get; private set; }
		public double Y { get; private set; }

		public PointD(double x, double y)
		{
			X = x;
			Y = y;
		}

		public static double operator *(PointD p1, PointD p2)
		{
			return p1.X*p2.Y - p1.Y*p2.X;

		}
		public static PointD operator *(double d, PointD p)
		{
			return new PointD(d*p.X, d*p.Y);

		}
		public static PointD operator *(PointD p, double d )
		{
			return new PointD(d*p.X, d*p.Y);

		}

		public static  PointD operator -(PointD p1, PointD p2)
		{
			return new PointD(p1.X - p2.X, p1.Y - p2.Y);
		}
		public static  PointD operator +(PointD p1, PointD p2)
		{
			return new PointD(p1.X + p2.X, p1.Y + p2.Y);
		}

		#region Generated

		public bool Equals(PointD other)
		{
			if (ReferenceEquals(null, other)) return false;
			if (ReferenceEquals(this, other)) return true;
			return Utilits.AreApproxEqual(X, other.X) && Utilits.AreApproxEqual(Y, other.Y);
		}

		public override bool Equals(object obj)
		{
			if (ReferenceEquals(null, obj)) return false;
			if (ReferenceEquals(this, obj)) return true;
			if (obj.GetType() != typeof (PointD)) return false;
			return Equals((PointD) obj);
		}

		public override int GetHashCode()
		{
			unchecked
			{
//				return (X.GetHashCode() * 397) ^ Y.GetHashCode();
				return (Math.Round(X, Utilits.EqDigitsCount).GetHashCode() * 397) ^ Math.Round(Y, Utilits.EqDigitsCount).GetHashCode();
			}
		}

		public override string ToString()
		{
			return string.Format("X: {0}, Y: {1}", X, Y);
		}
		public static bool operator ==(PointD a, PointD b)
		{
			// If both are null, or both are same instance, return true.
			if (System.Object.ReferenceEquals(a, b))
			{
				return true;
			}

			// If one is null, but not both, return false.
			if (((object)a == null) || ((object)b == null))
			{
				return false;
			}

			// Return true if the fields match:
			return Utilits.AreApproxEqual(a.X, b.X) && Utilits.AreApproxEqual(b.Y, a.Y);
		}

		public static bool operator !=(PointD a, PointD b)
		{
			return !(a == b);
		}

		#endregion

	}

	[TestFixture]
	internal class PointDTester
	{
		public object[] Vectors = {
		                           	new object[] {new PointD(1, 1), new PointD(1, -1), new PointD(2, 0)}
		                           };
		[Test]
		[TestCaseSource("Vectors")]
		public void OperatorsTest(PointD v1, PointD v2, PointD res)
		{
			Assert.That(v1+v2, Is.EqualTo(res));
			Assert.That(res - v2, Is.EqualTo(v1));
			Assert.That(res - v1, Is.EqualTo(v2));
		}

		public object[] VMultipliers = {
		                               	new object[]{new PointD(1, 2), 2, new PointD(2, 4)}
		                               };
		[Test]
		[TestCaseSource("VMultipliers")]
		public void MultiplierTest(PointD v1, double d, PointD res)
		{
			Assert.That(v1*d, Is.EqualTo(res));
			Assert.That(d * v1, Is.EqualTo(res));

		}
	}
}
