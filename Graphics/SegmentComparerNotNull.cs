﻿using NUnit.Framework;

namespace PolygonIntersections
{
	public class SegmentComparerNotNull : SegmentComparer
	{
		public SegmentComparerNotNull(double x = 0)
		{
			X = x;
		}

		/// <summary>
		/// x &lt; y; -1 &lt; 0 <br/>
		/// x = y;  0 = 0<br/>
		/// x > y;  1 &gt; 0<br/>
		/// </summary>
		public override int Compare(Segment x, Segment y)
		{
			var res = base.Compare(x, y);
			if (res == 0)
			{
				if (x.GetSlope() > y.GetSlope())
				{
					return 1;
				}
				if (x.GetSlope() < y.GetSlope())
				{
					return -1;
				}
				return 0;
			}
			return res;
		}

		public override string ToString()
		{
			return string.Format("X: {0}", X);
		}
	}

	[TestFixture]
	internal class SegmentComparerNotNullTester
	{
		public object[] ComareTestSourse =

			{
				new object[] {1, new Segment(0,0,2,2), new Segment(0,2, 1.5,0), 1},
				new object[] {1.5, new Segment(0,0,2,2), new Segment(0,2, 2,0), 1},
				new object[] {0.5, new Segment(0,0,2,2), new Segment(0,2, 2,0), -1},
				new object[] {1, new Segment(0,0,2,2), new Segment(0,2, 2,0), 0},

			};
		[Test]
		[TestCaseSource("ComareTestSourse")]
		public void CompareTest(double currentX, Segment x, Segment y, int res)
		{
			var comp = new SegmentComparerNotNull(currentX);
			Assert.That(comp.Compare(x, y), Is.EqualTo(res));
			Assert.That(comp.Compare(y, x), Is.EqualTo(-res));
		}
	}
}
