﻿namespace PolygonIntersections
{
	class PointEvent
	{
		public PointEventType Type;
		public PointD Point;
		public Segment S;
		public Segment S1;

		public override string ToString()
		{
			return string.Format("Type: {0}, Point: {1}, S: {2}, S1: {3}", Type, Point, S, S1);
		}
	}


	internal enum PointEventType
	{
		Begin, End, Intersection, Vertical
	}
}
