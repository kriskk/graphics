﻿using System;
using System.Diagnostics;
using NUnit.Framework;

namespace PolygonIntersections
{
	public class Segment
	{
		public PointD Start { get; private set; }
		public PointD End { get; private set; }
		public PointD Vector
		{
			get { return End - Start; }
		}

		public double Top { get { return Math.Max(Start.Y, End.Y); } }
		public double Bottom { get { return Math.Min(Start.Y, End.Y); } }
		public double Right { get { return Math.Max(Start.X, End.X); } }
		public double Left { get { return Math.Min(Start.X, End.X); } }

		public Segment(PointD start, PointD end)
		{
			Start = start;
			End = end;
		}
		public Segment(Segment old)
			:this(old.Start.X,old.Start.Y,old.End.X,old.End.Y)
		{

		}

		public Segment(double x1, double y1, double x2, double y2)
			:this(new PointD(x1,y1), new PointD(x2,y2))
		{
		}
		public bool IsEdge(PointD p)
		{
			return Start.Equals(p) || End.Equals(p);
		}
		public bool IsVertical()
		{
			return Utilits.AreApproxEqual(Left, Right);
		}
		private bool RectanglesIntersect(Segment s)
		{
			return !(Right <= s.Left ||
			        Left >= s.Right ||
			        Top <= s.Bottom ||
			        Bottom >= s.Top);

		}
		private bool IntersectsByPoints(PointD a,PointD b,PointD c,PointD d)
		{
			return ((b - a)*(c - a))*((b - a)*(d - a)) <= 0 &&
			       ((d - c)*(b - c))*((d - c)*(a - c)) <= 0;
		}

		private bool Intersects(Segment s)
		{
			return IntersectsByPoints(Start, End, s.Start, s.End);
		}

		public PointD GetIntersectionPoint(Segment s)
		{
			if (s == null) return null;
			if (!Intersects(s)) return null;
			var ta = (s.Start - this.Start) * s.Vector;
			var tb = (this.Vector*s.Vector);
			Debug.Assert(tb.IsNormal(), "Bad constant");
			if (Math.Abs(tb) < Utilits.EqEpsilon)
				return null; // parallel lines
//			if (Math.Abs(ta) < Utilits.EqEpsilon)
//				return null; // colinear or never intersect
			var t = ta/tb;
			if (!(0 <= t && t <= 1))
				return null; // IsIntersect + I dont need edge intersections
			var ip = this.Start + (this.Vector*t);
			if(s.IsEdge(ip) && IsEdge(ip))
				return null;
			return ip;
		}
		public PointD GetIntersectionPoint(double abscissaX)
		{
			if (IsVertical())
				return null;
			if (Utilits.AreApproxEqual(abscissaX, End.X))
				return End;
			if (Utilits.AreApproxEqual(abscissaX, Start.X))
				return Start;
			var verticalLine = new Segment(abscissaX, Bottom - 1, abscissaX, Top + 1);
			return this.GetIntersectionPoint(verticalLine);
		}
		public double GetSlope()
		{
			var delim = End.X - Start.X;
			if (Utilits.AreApproxEqual(delim ,0 )) 
				throw new NotFiniteNumberException();
			return (End.Y - Start.Y)/delim;
		}

		#region Generated

		public bool Equals(Segment other)
		{
			if (ReferenceEquals(null, other)) return false;
			if (ReferenceEquals(this, other)) return true;
			return Equals(other.Start, Start) && Equals(other.End, End);
		}

		public override bool Equals(object obj)
		{
			if (ReferenceEquals(null, obj)) return false;
			if (ReferenceEquals(this, obj)) return true;
			if (obj.GetType() != typeof (Segment)) return false;
			return Equals((Segment) obj);
		}

		public override int GetHashCode()
		{
			unchecked
			{
				return ((Start != null ? Start.GetHashCode() : 0)*397) ^ (End != null ? End.GetHashCode() : 0);
			}
		}

		public override string ToString()
		{
			return string.Format("[{0}:{1}->{2}:{3}]", Start.X, Start.Y, End.X, End.Y);
		}

		#endregion

	}

	[TestFixture]
	internal class SegmentTester
	{
		public object[] Gipt =
			{
				new object[] {new Segment(0, 0, 2, 2), new Segment(0, 1, 1, 0), new PointD(0.5, 0.5)},
				new object[] {new Segment(0, 0, 1, 0), new Segment(2,0, 1,0), null},
				new object[] {new Segment(0, 0, 1, 0), new Segment(1,0, 2,0), null},
				new object[] {new Segment(0, 0, 1, 1), new Segment(1,1, 2,0), null},
				new object[] {new Segment(0, 0, 1, 1), new Segment(0, 2, 2, 0), new PointD(1,1)},
				new object[] {new Segment(0, 0, 1, 1), new Segment(0,5,5,0), null},
				new object[] {new Segment(2,5,2,9), new Segment(0,0,5,0), null},
				new object[] {new Segment(2,9,2,5), new Segment(0,0,5,0), null},
				new object[] {new Segment(4,8,4,11), new Segment(3,5,6,1), null},
				new object[] {new Segment(4,8,4,11), new Segment(3,5,6,5), null},
				new object[] {new Segment(4,8,4,11), new Segment(6,1,3,5), null},
				new object[] {new Segment(4,11,4,8), new Segment(3,5,6,1), null},
				new object[] {new Segment(3,5,6,1), new Segment(4,11,4,8), null},
				new object[] {new Segment(4,11,4,8), new Segment(3,5,6,5), null},
				new object[] {new Segment(4,11,4,8), new Segment(6,1,3,5), null},
				new object[] {new Segment(0,2,8,2), new Segment(2,2,3,4), new PointD(2,2)},
				new object[] {new Segment(2,2,3,4), new Segment(0,2,8,2), new PointD(2,2)},

			};
		[Test]
		[TestCaseSource("Gipt")]
		public void GetIntertsectionPointTest(Segment s1, Segment s2, PointD ip)
		{
			Assert.That(s1.GetIntersectionPoint(s2), Is.EqualTo(ip));
		}

		public object[] Gipwv =
			{
				new object[] {new Segment(0, 0, 2, 2), 1, new PointD(1, 1)},
				new object[] {new Segment(0, 0, 2, 2), 0, new PointD(0,0)},
				new object[] {new Segment(0, 0, 2, 2), 2, new PointD(2,2)},
				new object[] {new Segment(0, 0, 0, 2), 1, null},
				new object[] {new Segment(0, 0, 0, 2), 2, null},
				new object[] {new Segment(0, 0, 0, 2), 0, null},
			};
		[Test]
		[TestCaseSource("Gipwv")]
		public void GetIpWithVerticalTest(Segment s, double abscisse, PointD ip)
		{
			Assert.That(s.GetIntersectionPoint(abscisse), Is.EqualTo(ip));
		}

		[Test]
		public void SlopeTest()
		{
			var slopeSegs = new Segment[]
			                	{
			                		new Segment(1,1,2,2), 
			                		new Segment(3,3,2,2), 
			                		new Segment(5,0,0,9), 
			                		new Segment(1,9,9,0), 
			                	};
			foreach (var seg in slopeSegs)
			{
				Console.Out.WriteLine(seg.GetSlope());
			}
		}
	}
}
