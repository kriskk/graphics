﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using NUnit.Framework;

namespace PolygonIntersections
{
	public class ImplicitTreap<T>
	{
		#region initialize

		private readonly int y;
		public T Info;

		public ImplicitTreap<T> Left;
		public ImplicitTreap<T> Right;
		public int Size = 1;
		public int RightPos { get { return SizeOf(Left) + 1; } }
		public int CurrentPos { get { return SizeOf(Left); } }

		public ImplicitTreap(T cost)
			: this(Utilits.Rand.Next(), cost)
		{
		}

		private ImplicitTreap(int y, T info, ImplicitTreap<T> left = null, ImplicitTreap<T> right = null)
		{
			this.y = y;
			this.Info = info;
			this.Left = left;
			this.Right = right;
		}

		#endregion

		#region mainFuncs
		public T this[int pos]
		{
			get
			{
				if (pos < 0 || pos > Size)
					throw new ArgumentOutOfRangeException();
				if (pos == CurrentPos)
					return this.Info;
				if (pos < CurrentPos)
					return Left[pos];
				return Right[pos - RightPos];
			}
		}

		public static int SizeOf(ImplicitTreap<T> treap)
		{
			return treap == null ? 0 : treap.Size;
		}

		public void Recalc()
		{
			Size = SizeOf(Left) + SizeOf(Right) + 1;
		}

		public static ImplicitTreap<T> Merge(ImplicitTreap<T> L, ImplicitTreap<T> R)
		{
			if (L == null) return R;
			if (R == null) return L;

			if (L.y > R.y)
			{
				var newR = Merge(L.Right, R);
				var ans =  new ImplicitTreap<T>(L.y, L.Info, L.Left, newR);
				ans.Recalc();
				return ans;
			}
			else
			{
				var newL = Merge(L, R.Left);
				var ans =  new ImplicitTreap<T>(R.y, R.Info, newL, R.Right);
				ans.Recalc();
				return ans;
			}
		}

		public void Split(int pos, out ImplicitTreap<T> L, out ImplicitTreap<T> R)
		{
			ImplicitTreap<T> newTree = null;
			int curIndex = SizeOf(Left) + 1;

			if (curIndex <= pos)
			{
				if (Right == null)
					R = null;
				else
					Right.Split(pos - curIndex, out newTree, out R);
				L = new ImplicitTreap<T>(y, Info, Left, newTree);
				L.Recalc();
			}
			else
			{
				if (Left == null)
					L = null;
				else
					Left.Split(pos, out L, out newTree);
				R = new ImplicitTreap<T>(y, Info, newTree, Right);
				R.Recalc();
			}
//			Debug.Assert(SizeOf(L) == pos,"L Bug");
//			Debug.Assert(SizeOf(R) == Size - pos,"R Bug");
		}

		public ImplicitTreap<T> Add(int pos, T elemCost)
		{
			ImplicitTreap<T> l, r;
			Split(pos, out l, out r);
			ImplicitTreap<T> m = new ImplicitTreap<T>(Utilits.Rand.Next(), elemCost);
			return Merge(Merge(l, m), r);
		}

		public ImplicitTreap<T> Remove(int pos)
		{
			ImplicitTreap<T> l, m, r;
			Split(pos, out l, out r);
			r.Split(1, out m, out r);
			return Merge(l, r);
		}

		public static ImplicitTreap<T> CreateFromArray(T[] items)
		{
			var t = new ImplicitTreap<T>(items[0]);
			var ans = items.Skip(1).Aggregate(t, (current, item) => current.Add(0, item));
			ans.Recalc();
			return ans;
		}

		#endregion

		#region minmax

		internal ImplicitTreap<T> DigRight()
		{
			return Right != null ? Right.DigRight() : this;
		}

		internal ImplicitTreap<T> DigLeft()
		{
			return Left != null ? Left.DigLeft() : this;
		}

		#endregion


	
		#region Generated

		public IEnumerable<T> GetAllInfos()
		{
			if (Left != null)
				foreach (var info in Left.GetAllInfos())
				{
					yield return info;
				}
			yield return Info;
			if (Right != null)
			foreach (var info in Right.GetAllInfos())
			{
				yield return info;
			}
		}
		public IEnumerable<int> GetAllInfos(Dictionary<T,int> mapper)
		{
			return GetAllInfos().Select(s => mapper[s]);
		}

		public override string ToString()
		{
			return string.Format("{0} <{1} | {2}>", Info, Left, Right);
		}
		public string ToString(Dictionary<T,int> mapper)
		{
			if (Left == null && Right == null)
				return mapper[Info].ToString(CultureInfo.InvariantCulture);
			var leftS = Left == null ? "Q" :  Left.ToString(mapper);
			var rightS = Right == null ? "Q" : Right.ToString(mapper);
			return string.Format("{0} <{1} | {2}>", mapper[Info], leftS, rightS);
		}

		#endregion


	}

	public class ItemAndPos<T>
	{
		public ImplicitTreap<T> Item;
		public int Pos;
	}

	[TestFixture]
	internal class ImplicitTreapTester
	{
		public Segment[] Segments = new Segment[]
		                            	{
		                            		new Segment(0,1,5,1), 
		                            		new Segment(0,2,5,2), 
		                            		new Segment(0,3,5,3), 
		                            		new Segment(0,4,5,4), 
		                            	};
		
		[Test]
		public void CreateTest()
		{
			var t = ImplicitTreap<Segment>.CreateFromArray(Segments);
			var q = t.GetAllInfos().ToArray();
			foreach (var segment in q)
			{
				Console.Out.WriteLine(segment);
			}
			for (int i = 0; i < t.Size; i++)
			{
				Console.WriteLine(t[i]);				
			}
		}

		[Test]
		public void AddTest()
		{
			var t = ImplicitTreap<Segment>.CreateFromArray(Segments.Reverse().ToArray());
			var ns = new Segment(1, 55, 3, 4);
			for (int i = 0; i < t.Size+3; i++)
			{
				var nt = t.Add(i, ns);
				Console.Out.WriteLine(i + " - "+String.Join(" ", nt.GetAllInfos().Select(info => info.Start.Y)));
			}
		}
	}
}
