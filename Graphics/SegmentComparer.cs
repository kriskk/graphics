﻿using System.Collections.Generic;
using System.Diagnostics;
using NUnit.Framework;

namespace PolygonIntersections
{
	public class SegmentComparer :Comparer<Segment>
	{
		public double X;

		public SegmentComparer(double x = 0)
		{
			X = x;
		}

		/// <summary>
		/// x &lt; y; -1 &lt; 0 <br/>
		/// x = y;  0 = 0<br/>
		/// x > y;  1 &gt; 0<br/>
		/// </summary>
		public override int Compare(Segment x, Segment y)
		{
			if (Equals(x, y)) return 0;
			Debug.Assert(x != null);
			Debug.Assert(y != null);
			var ip1 = x.GetIntersectionPoint(X);
			Debug.Assert(ip1 != null, "x Uncomparable");
			var ip2 = y.GetIntersectionPoint(X);
			Debug.Assert(ip2 != null, "y Uncomparable");
			var y1 = ip1.Y;
			var y2 = ip2.Y;
			if (Utilits.AreApproxEqual(y1, y2))
				return 0;
			return (y1 < y2) ? -1 : 1;
		}

		public override string ToString()
		{
			return string.Format("X: {0}", X);
		}
	}

	[TestFixture]
	internal class SegmentComparerTester
	{
		public object[] ComareTestSourse =

			{
				new object[] {1, new Segment(0,0,2,2), new Segment(0,2, 1.5,0), 1},
				new object[] {1.5, new Segment(0,0,2,2), new Segment(0,2, 2,0), 1},
				new object[] {0.5, new Segment(0,0,2,2), new Segment(0,2, 2,0), -1},
				new object[] {1, new Segment(0,0,2,2), new Segment(0,2, 2,0), 0},

			};
		[Test]
		[TestCaseSource("ComareTestSourse")]
		public void CompareTest(double currentX, Segment x, Segment y, int res)
		{
			var comp = new SegmentComparer(currentX);
			Assert.That(comp.Compare(x,y), Is.EqualTo(res));
			Assert.That(comp.Compare(y,x), Is.EqualTo(-res));
		} 
	}
}
