﻿using System;
using NUnit.Framework;

namespace PolygonIntersections
{
	static public class Utilits
	{
		static public readonly double EqEpsilon = 1e-7;
		public static readonly int EqDigitsCount = 6;
		static public readonly double DEpsilon = 1e-9;

		static public Random Rand = new Random(1359483);

		static public bool IsNormal(this double d)
		{
			return !Double.IsNaN(d) && !Double.IsInfinity(d);
		}
		static public bool AreApproxEqual(double d1, double d2)
		{
			return Math.Abs(d1 - d2) < EqEpsilon;
		}
		static public T[] Shuffle<T>(this T[] source)
		{
			T[] a = new T[source.Length];
			for (int i = 0; i < source.Length; i++)
			{
				var j = Rand.Next(i+1);
				a[i] = a[j];
				a[j] = source[i];
			}
			return a;
		}



	}

	[TestFixture]
	internal class UtilitsTester
	{
		[Test]
		public void ShuffleTest()
		{
			var a = new[] {1, 2, 3, 4, 5, 6, 7};
			for (int i = 0; i < 5; i++)
			{
				Console.Out.WriteLine(String.Join(" ", a.Shuffle()));
			}
		} 
	}
}
