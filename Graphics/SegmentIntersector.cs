﻿using System;
using System.Collections.Generic;
using System.Linq;
using NGenerics.DataStructures.Queues;
using NUnit.Framework;

namespace PolygonIntersections
{
	public static class Log
	{
		public static int I = 0;
		public static Dictionary<Segment, int> Mapper;
		public static void CreateMapper(IEnumerable<Segment> segments)
		{
			int i = 0;
			Mapper = segments.ToDictionary(s => s, s => i++);
		}
	}

	static class SegmentIntersector
	{
		public static IEnumerable<PointD> IntersectSimple(Segment[] segments)
		{
			var intersections = new HashSet<PointD>();
			for (int i = 0; i < segments.Length; i++)
			{
				for (int j = i+1; j < segments.Length; j++)
				{
					var s1 = segments[i];
					var s2 = segments[j];
					var ip = s1.GetIntersectionPoint(s2);
					if (ip != null && !intersections.Contains(ip))
						intersections.Add(ip);
				}
			}
			return intersections;
		}

		public static HashSet<PointD> Intersect(Segment[] segments)
		{
			var intersections = new HashSet<PointD>();
			var queue = new PriorityQueue<PointEvent, double>(PriorityQueueType.Minimum);
			var sweep = new Sweep();
			foreach (var segment in segments)
			{
				if (Utilits.AreApproxEqual(segment.Start.X, segment.End.X))
				{
					queue.Enqueue(new PointEvent
					              	{
					              		Type = PointEventType.Vertical, 
										S = segment
					              	}, segment.Left);
				}
				else
				{
					queue.Enqueue(new PointEvent
					              	{
					              		Type = PointEventType.Begin,
					              		S = segment
									}, segment.Left + Utilits.DEpsilon);
					queue.Enqueue(new PointEvent
					              	{
					              		Type = PointEventType.End,
					              		S = segment
					              	}, segment.Right - Utilits.DEpsilon);

				}
			}
			while (queue.Count != 0)
			{
				Console.Out.WriteLine(Log.I++);
				PointEvent pEvent = queue.Dequeue();
				var q = sweep.X + ": " +String.Join(" ",sweep.GetAllSegments(Log.Mapper));
				switch (pEvent.Type)
				{
					case (PointEventType.Vertical):
						{
							var s = pEvent.S;
							sweep.X = s.Left;
							foreach (var seg  in sweep.GetAllSegments())
							{
								var ip = seg.GetIntersectionPoint(s);
								if (ip != null && !intersections.Contains(ip))
								{
									intersections.Add(ip);
								}
							}
							break;
						}
					case(PointEventType.Begin):
						{
							var s = pEvent.S;
							sweep.X = s.Left;
							sweep.Add(s);
//							var sc = sweep.GetCeilingSegment(s);
//							var sf = sweep.GetFloorSegment(s);
							var pc = sweep.GetCeiling(s);
							var pf = sweep.GetFloor(s);
							for (int i = pf; i <= pc; i++)
							{
								AddIntesectionPoint(s, sweep.GetSegment(i), intersections, queue);
							}
							break;
						}
					case (PointEventType.End):
						{
							var s = pEvent.S;
							sweep.X = s.Right;
							var s1 = sweep.GetCeilingSegment(s);
							var s2 = sweep.GetFloorSegment(s);
							sweep.Remove(s);
							AddIntesectionPoint(s1, s2, intersections, queue);
							break;
						}
					case (PointEventType.Intersection):
						{
							sweep.X = pEvent.Point.X;
							var s1 = pEvent.S;
							var s2 = pEvent.S1;
							OrderSegments(ref s1, ref s2); // now s1 - \ ; s2 - /
							var s3 = sweep.GetCeilingSegment(s1);
							var s4 = sweep.GetFloorSegment(s2);
							AddIntesectionPoint(s1, s4, intersections,queue);
							AddIntesectionPoint(s2, s3, intersections,queue);
							sweep.CheckAndSwap(s1,s2);
							break;
						}
				}
			}

			return intersections;
		}
		private static void AddIntesectionPoint(Segment s1, Segment s2, 
			HashSet<PointD> intersections, PriorityQueue<PointEvent, double> queue)
		{
			if (s1 == null || s2 == null) return;
			var ip = s1.GetIntersectionPoint(s2);
			if (ip == null) return;
			if (!intersections.Contains(ip))
			{
				intersections.Add(ip);
				queue.Enqueue(new PointEvent  // если вынести из if  происходит зацикливание, плакали мои пересечение 3-х отрезков в одной точке
				              	{
				              		Type = PointEventType.Intersection,
				              		S = s1,
				              		S1 = s2,
				              		Point = ip
				              	}, ip.X);
			}
		}

		internal static void OrderSegments(ref Segment s1, ref Segment s2)
		{
			if (s2.GetSlope() < s1.GetSlope())
			{
				var temp = s1;
				s1 = s2;
				s2 = temp;
			}
		}

	}

	[TestFixture]
	internal class SegmentIntersectorTester
	{
		[Test]
		public void OrderSegsTest()
		{
			var segs = new[]
			           	{
			           		new Segment(0, 0, 3, 3),
			           		new Segment(0, 3, 3, 0),
			           	};
			var order = new[] {new {s1 = 0, s2 = 1}, new {s1 = 1, s2 = 0}};
			foreach (var ord in order)
			{
				Segment s1 = segs[ord.s1];
				Segment s2 = segs[ord.s2];
				SegmentIntersector.OrderSegments(ref s1,ref s2);
				Assert.That(s1, Is.EqualTo(segs[1]));
				Assert.That(s2, Is.EqualTo(segs[0]));
			}
		}

		#region poligons

		public static Poligon P1 = new Poligon(new[]
		                                       	{
		                                       		new PointD(0, 1),
		                                       		new PointD(0, 5),
		                                       		new PointD(3, 3),
		                                       	});

		public static Poligon P2 = new Poligon(new[]
		                                       	{
		                                       		new PointD(2, 5),
		                                       		new PointD(5, 5),
		                                       		new PointD(6, 1),
		                                       		new PointD(3, 1),
		                                       	});

		public static Poligon P3 = new Poligon(new[]
		                                       	{
		                                       		new PointD(1, 12),
		                                       		new PointD(14, 12),
		                                       		new PointD(6, 3),
		                                       	});

		public static Poligon P4 = new Poligon(new[]
		                                       	{
		                                       		new PointD(6, 1),
		                                       		new PointD(10, 5),
		                                       		new PointD(7, 5),
		                                       		new PointD(11, 7),
		                                       		new PointD(8, 7),
		                                       		new PointD(12, 8),
		                                       		new PointD(12, 11),
		                                       		new PointD(10, 13),
		                                       		new PointD(10, 11),
		                                       		new PointD(3, 13),
		                                       		new PointD(4, 11),
		                                       		new PointD(4, 8),
		                                       		new PointD(2, 8),
		                                       		new PointD(6, 5),
		                                       		new PointD(3, 5),
		                                       	});

		public static Poligon P5 = new Poligon(new[]
		                                       	{
		                                       		new PointD(0, 2),
		                                       		new PointD(4, 6),
		                                       		new PointD(8, 2),
		                                       	});

		public static Poligon P7 = new Poligon(new[]
		                                       	{
		                                       		new PointD(0.1, 2.1),
		                                       		new PointD(2, 4),
		                                       		new PointD(4, 2),
		                                       	});

		public static Poligon P6 = new Poligon(new[]
		                                       	{
		                                       		new PointD(2, 2),
		                                       		new PointD(3, 4),
		                                       		new PointD(6, 4),
		                                       		new PointD(6, 0),
		                                       		new PointD(3, 0),
		                                       		new PointD(2, 2),
		                                       		new PointD(3, 1),
		                                       		new PointD(5, 1),
		                                       		new PointD(2, 2),
		                                       		new PointD(5, 3),
		                                       		new PointD(3, 3),
		                                       	});

		public static Poligon P8 = new Poligon(new[]
		                                       	{
		                                       		new PointD(2, 2),
		                                       		new PointD(1.5, 3),
		                                       		new PointD(6, 4),
		                                       		new PointD(6, 0),
		                                       		new PointD(3, 0),
		                                       		new PointD(2, 2),
		                                       		new PointD(3, 1),
		                                       		new PointD(5, 1),
		                                       		new PointD(2, 2),
		                                       		new PointD(5, 3),
		                                       		new PointD(3, 3),
		                                       	});
		public static Poligon Star = new Poligon(new[]
		                                       	{
		                                       		new PointD(1, 0),
		                                       		new PointD(4, 6),
		                                       		new PointD(5, 0),
		                                       		new PointD(0, 7),
		                                       		new PointD(8, 3),
		                                       	});
		public static Poligon Pentagon = new Poligon(new[]
		                                       	{
		                                       		new PointD(0, 1),
		                                       		new PointD(-1, 4),
		                                       		new PointD(2, 7),
		                                       		new PointD(6, 5),
		                                       		new PointD(6, 1),
		                                       	});

		public static object[] tcs = new[]
		                             	{
		                             		new object[] {P1, P2},
		                             		new object[] {P3, P4},
		                             		new object[] {P5, P6},
		                             		new object[] {P5, P7},
		                             		new object[] {P7, P8},
		                             		new object[] {Star, Pentagon},

		                             	};

		public static readonly object[] polygons = new[]
		                                           	{
		                                           		P1, P2, P3, P4, P5, P6
		                                           	};

		#endregion

		[Test]
//		[TestCaseSource("tcs")]
		[Combinatorial]
		public void IntersectionsTestSimple([ValueSource("polygons")]Poligon p1, [ValueSource("polygons")]Poligon p2)
		{
			if(p1 == p2) return;
//			var p1 = P1;
//			var p2 = P2;

			Segment[] segments = Poligon.GetAllSegments(p1, p2).ToArray();
			Log.CreateMapper(segments);
			foreach (var pair in Log.Mapper.ToArray())
			{
				Console.Out.WriteLine(pair.Key + " " +pair.Value);
			}
			var simpleIntersections = SegmentIntersector.IntersectSimple(segments);
			int i = 1;
			foreach (var ip in simpleIntersections.OrderBy(ip => ip.X))
			{
				Console.Out.WriteLine(i++ + " " + ip);
			}
		}

		
		[Test]
		[TestCaseSource("tcs")]
		public void IntersectionsTest(Poligon p1, Poligon p2)
		{
			var segments = Poligon.GetAllSegments(p1, p2).ToArray();
			Log.CreateMapper(segments);
			Log.I = 0;
			var intersections = SegmentIntersector.Intersect(segments).OrderBy(ip => ip.X).ToList();
			foreach (var ip in intersections)
			{
				Console.Out.WriteLine(ip);
			}
			var simpleIntersections = SegmentIntersector.IntersectSimple(Poligon.GetAllSegments(p1, p2).ToArray());
			foreach (var ip in simpleIntersections)
			{
				Assert.That(intersections.Contains(ip), "Has no "+ip);
			}
		}
	}

	
}
