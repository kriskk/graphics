﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using NUnit.Framework;

namespace PolygonIntersections
{
	class Sweep
	{
		private SegmentComparer Comparer = new SegmentComparer();
		private SegmentComparerNotNull ComparerNotNull = new SegmentComparerNotNull();
		private ImplicitTreap<Segment> treap;

		public double X
		{
			get { return Comparer.X; }
			set
			{
				Comparer.X = value;
				ComparerNotNull.X = value;
			}
		}

		public Sweep()
		{
		}
		public void Add(Segment s)
		{
			if (treap == null)
			{
				treap = new ImplicitTreap<Segment>(s);
			}
			else
			{
				var oldSize = treap.Size;
				int pos = treap.FindNotNull(s, ComparerNotNull);
				treap = treap.Add(pos, s);
//				treap.Recalc();
				Debug.Assert(ImplicitTreap<Segment>.SizeOf(treap) == oldSize+1);
			}

		}
		public void Remove(Segment s)
		{
			if (treap == null) return;
			var oldSize = treap.Size;
			treap = treap.Remove(s,Comparer);
			Debug.Assert(ImplicitTreap<Segment>.SizeOf(treap) == oldSize - 1);
		}
		public Segment GetCeilingSegment(Segment s)
		{
			if (treap == null) return null;
			var pos = treap.GetCeilPos(s,Comparer);
				if (!pos.HasValue || pos < 0 || pos >= treap.Size) return null;
			return treap[pos.Value];
		}
		public Segment GetFloorSegment(Segment s)
		{
			if (treap == null) return null;
			var pos = treap.GetFloorPos(s,Comparer);
			if (pos < 1 || pos >= treap.Size) return null;
			return treap[pos - 1];
		}
		public int GetCeiling(Segment s)
		{
			if (treap == null) return 0;
			var pos = treap.GetCeilPos(s,Comparer);
			if (!pos.HasValue) return treap.Size;
			if(pos < 0) return 0;
			if (pos >= treap.Size) return treap.Size;
			return pos.Value;
		}
		public int GetFloor(Segment s)
		{
			if (treap == null) return 0;
			var pos = treap.GetFloorPos(s,Comparer);
			if (pos < 1) return 0;
			if (pos >= treap.Size) return treap.Size - 1;
			return pos - 1;
		}
		public Segment GetSegment(int pos)
		{
			if (treap == null) return null;
			if (pos < 0 || pos >= treap.Size) return null;
			return treap[pos];
		}

		public void CheckAndSwap(Segment s1, Segment s2)
		{
			if (treap == null) return;
			Debug.Assert(s1.GetSlope() < s2.GetSlope(),"Wrong segment order"); // s1 was higher before intersection
			var pos1 = treap.Find(s1, Comparer);
			var pos2 = treap.Find(s2, Comparer);
			if (!pos1.HasValue) return ;
			if (!pos2.HasValue) return ;
			if (pos2 < pos1)
				treap = treap.Swap(s1, s2, Comparer);
		}

		public IEnumerable<Segment> GetAllSegments()
		{
			if ( treap==null )return new Segment[]{};
			return treap.GetAllInfos();
		}
		public IEnumerable<int> GetAllSegments(Dictionary<Segment, int> d)
		{
			if ( treap==null )return new int[]{};
			return treap.GetAllInfos(d);
		}

		public override string ToString()
		{
			return string.Format("X: {0}", Comparer.X);
		}
	}

	[TestFixture]
	internal class SweepTester
	{
		[Test]
		public void CheckNSwapTest()
		{
			var segs = new Segment[]
			           	{
			           		new Segment(0, 0, 3, 3),
			           		new Segment(0, 3, 3, 0),
			           	};
			var sweep = CreateFromRange(0.5, segs);
			Console.Out.WriteLine(GetAllI(sweep));
			sweep.CheckAndSwap(segs[1], segs[0]);
			Console.Out.WriteLine(GetAllI(sweep));
		}
		private Sweep CreateFromRange(double x, Segment[] segs)
		{
			var sweep = new Sweep();
			sweep.X = x;
			foreach (var segment in segs)
			{
				sweep.Add(segment);
			}
			return sweep;
		}
		public static string GetAllI(Sweep t)
		{
			return String.Join(" ", t.GetAllSegments().Select(i => i.Start.Y));
		}
	}
}
