﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PolygonDrawer
{
	class Triangle :Polygon
	{
		public Triangle(DPoint3d[] points) : base(points)
		{
			if (points.Length != 3)
				throw new ArgumentException("Must be 3 points");
		}
		public Triangle(DPoint3d a, DPoint3d b ,DPoint3d c)
			:this(new[]{a,b,c})
		{
		}

		public override IEnumerable<Polygon> GetAllPolygons()
		{
			yield return this;
		}
	}
}
