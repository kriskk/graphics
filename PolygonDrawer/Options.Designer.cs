﻿namespace PolygonDrawer
{
	partial class Options
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.Cube = new System.Windows.Forms.GroupBox();
			this.Conus = new System.Windows.Forms.GroupBox();
			this.label1 = new System.Windows.Forms.Label();
			this.label2 = new System.Windows.Forms.Label();
			this.label3 = new System.Windows.Forms.Label();
			this.label4 = new System.Windows.Forms.Label();
			this.AX = new System.Windows.Forms.NumericUpDown();
			this.AY = new System.Windows.Forms.NumericUpDown();
			this.AZ = new System.Windows.Forms.NumericUpDown();
			this.BX = new System.Windows.Forms.NumericUpDown();
			this.BY = new System.Windows.Forms.NumericUpDown();
			this.BZ = new System.Windows.Forms.NumericUpDown();
			this.CX = new System.Windows.Forms.NumericUpDown();
			this.CY = new System.Windows.Forms.NumericUpDown();
			this.CZ = new System.Windows.Forms.NumericUpDown();
			this.DX = new System.Windows.Forms.NumericUpDown();
			this.DY = new System.Windows.Forms.NumericUpDown();
			this.DZ = new System.Windows.Forms.NumericUpDown();
			this.ACZ = new System.Windows.Forms.NumericUpDown();
			this.ACY = new System.Windows.Forms.NumericUpDown();
			this.ACX = new System.Windows.Forms.NumericUpDown();
			this.AC = new System.Windows.Forms.Label();
			this.label5 = new System.Windows.Forms.Label();
			this.OCZ = new System.Windows.Forms.NumericUpDown();
			this.OCY = new System.Windows.Forms.NumericUpDown();
			this.OCX = new System.Windows.Forms.NumericUpDown();
			this.label6 = new System.Windows.Forms.Label();
			this.RC = new System.Windows.Forms.NumericUpDown();
			this.SaveButon = new System.Windows.Forms.Button();
			this.CancelButton = new System.Windows.Forms.Button();
			this.Cube.SuspendLayout();
			this.Conus.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.AX)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.AY)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.AZ)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.BX)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.BY)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.BZ)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.CX)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.CY)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.CZ)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.DX)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.DY)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.DZ)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.ACZ)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.ACY)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.ACX)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.OCZ)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.OCY)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.OCX)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.RC)).BeginInit();
			this.SuspendLayout();
			// 
			// Cube
			// 
			this.Cube.Controls.Add(this.DZ);
			this.Cube.Controls.Add(this.DY);
			this.Cube.Controls.Add(this.DX);
			this.Cube.Controls.Add(this.CZ);
			this.Cube.Controls.Add(this.CY);
			this.Cube.Controls.Add(this.CX);
			this.Cube.Controls.Add(this.BZ);
			this.Cube.Controls.Add(this.BY);
			this.Cube.Controls.Add(this.BX);
			this.Cube.Controls.Add(this.AZ);
			this.Cube.Controls.Add(this.AY);
			this.Cube.Controls.Add(this.AX);
			this.Cube.Controls.Add(this.label4);
			this.Cube.Controls.Add(this.label3);
			this.Cube.Controls.Add(this.label2);
			this.Cube.Controls.Add(this.label1);
			this.Cube.Location = new System.Drawing.Point(14, 23);
			this.Cube.Name = "Cube";
			this.Cube.Size = new System.Drawing.Size(265, 232);
			this.Cube.TabIndex = 0;
			this.Cube.TabStop = false;
			this.Cube.Text = "Cube";
			// 
			// Conus
			// 
			this.Conus.Controls.Add(this.RC);
			this.Conus.Controls.Add(this.label6);
			this.Conus.Controls.Add(this.label5);
			this.Conus.Controls.Add(this.OCZ);
			this.Conus.Controls.Add(this.OCY);
			this.Conus.Controls.Add(this.OCX);
			this.Conus.Controls.Add(this.AC);
			this.Conus.Controls.Add(this.ACZ);
			this.Conus.Controls.Add(this.ACY);
			this.Conus.Controls.Add(this.ACX);
			this.Conus.Location = new System.Drawing.Point(296, 26);
			this.Conus.Name = "Conus";
			this.Conus.Size = new System.Drawing.Size(220, 229);
			this.Conus.TabIndex = 1;
			this.Conus.TabStop = false;
			this.Conus.Text = "Conus";
			// 
			// label1
			// 
			this.label1.AutoSize = true;
			this.label1.Location = new System.Drawing.Point(14, 22);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(14, 13);
			this.label1.TabIndex = 0;
			this.label1.Text = "A";
			// 
			// label2
			// 
			this.label2.AutoSize = true;
			this.label2.Location = new System.Drawing.Point(14, 72);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(14, 13);
			this.label2.TabIndex = 1;
			this.label2.Text = "B";
			// 
			// label3
			// 
			this.label3.AutoSize = true;
			this.label3.Location = new System.Drawing.Point(14, 120);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(14, 13);
			this.label3.TabIndex = 2;
			this.label3.Text = "C";
			// 
			// label4
			// 
			this.label4.AutoSize = true;
			this.label4.Location = new System.Drawing.Point(14, 174);
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size(15, 13);
			this.label4.TabIndex = 3;
			this.label4.Text = "D";
			// 
			// AX
			// 
			this.AX.Location = new System.Drawing.Point(55, 19);
			this.AX.Maximum = new decimal(new int[] {
            300,
            0,
            0,
            0});
			this.AX.Minimum = new decimal(new int[] {
            300,
            0,
            0,
            -2147483648});
			this.AX.Name = "AX";
			this.AX.Size = new System.Drawing.Size(46, 20);
			this.AX.TabIndex = 4;
			// 
			// AY
			// 
			this.AY.Location = new System.Drawing.Point(127, 20);
			this.AY.Maximum = new decimal(new int[] {
            300,
            0,
            0,
            0});
			this.AY.Minimum = new decimal(new int[] {
            300,
            0,
            0,
            -2147483648});
			this.AY.Name = "AY";
			this.AY.Size = new System.Drawing.Size(46, 20);
			this.AY.TabIndex = 5;
			this.AY.Value = new decimal(new int[] {
            30,
            0,
            0,
            0});
			// 
			// AZ
			// 
			this.AZ.Location = new System.Drawing.Point(193, 20);
			this.AZ.Maximum = new decimal(new int[] {
            300,
            0,
            0,
            0});
			this.AZ.Minimum = new decimal(new int[] {
            300,
            0,
            0,
            -2147483648});
			this.AZ.Name = "AZ";
			this.AZ.Size = new System.Drawing.Size(46, 20);
			this.AZ.TabIndex = 6;
			// 
			// BX
			// 
			this.BX.Location = new System.Drawing.Point(55, 70);
			this.BX.Maximum = new decimal(new int[] {
            300,
            0,
            0,
            0});
			this.BX.Minimum = new decimal(new int[] {
            300,
            0,
            0,
            -2147483648});
			this.BX.Name = "BX";
			this.BX.Size = new System.Drawing.Size(46, 20);
			this.BX.TabIndex = 7;
			this.BX.Value = new decimal(new int[] {
            100,
            0,
            0,
            0});
			// 
			// BY
			// 
			this.BY.Location = new System.Drawing.Point(127, 70);
			this.BY.Maximum = new decimal(new int[] {
            300,
            0,
            0,
            0});
			this.BY.Minimum = new decimal(new int[] {
            300,
            0,
            0,
            -2147483648});
			this.BY.Name = "BY";
			this.BY.Size = new System.Drawing.Size(46, 20);
			this.BY.TabIndex = 8;
			this.BY.Value = new decimal(new int[] {
            30,
            0,
            0,
            0});
			// 
			// BZ
			// 
			this.BZ.Location = new System.Drawing.Point(193, 70);
			this.BZ.Maximum = new decimal(new int[] {
            300,
            0,
            0,
            0});
			this.BZ.Minimum = new decimal(new int[] {
            300,
            0,
            0,
            -2147483648});
			this.BZ.Name = "BZ";
			this.BZ.Size = new System.Drawing.Size(46, 20);
			this.BZ.TabIndex = 9;
			// 
			// CX
			// 
			this.CX.Location = new System.Drawing.Point(55, 113);
			this.CX.Maximum = new decimal(new int[] {
            300,
            0,
            0,
            0});
			this.CX.Minimum = new decimal(new int[] {
            300,
            0,
            0,
            -2147483648});
			this.CX.Name = "CX";
			this.CX.Size = new System.Drawing.Size(46, 20);
			this.CX.TabIndex = 10;
			// 
			// CY
			// 
			this.CY.Location = new System.Drawing.Point(127, 113);
			this.CY.Maximum = new decimal(new int[] {
            300,
            0,
            0,
            0});
			this.CY.Minimum = new decimal(new int[] {
            300,
            0,
            0,
            -2147483648});
			this.CY.Name = "CY";
			this.CY.Size = new System.Drawing.Size(46, 20);
			this.CY.TabIndex = 11;
			this.CY.Value = new decimal(new int[] {
            130,
            0,
            0,
            0});
			// 
			// CZ
			// 
			this.CZ.Location = new System.Drawing.Point(193, 113);
			this.CZ.Maximum = new decimal(new int[] {
            300,
            0,
            0,
            0});
			this.CZ.Minimum = new decimal(new int[] {
            300,
            0,
            0,
            -2147483648});
			this.CZ.Name = "CZ";
			this.CZ.Size = new System.Drawing.Size(46, 20);
			this.CZ.TabIndex = 12;
			// 
			// DX
			// 
			this.DX.Location = new System.Drawing.Point(55, 167);
			this.DX.Maximum = new decimal(new int[] {
            300,
            0,
            0,
            0});
			this.DX.Minimum = new decimal(new int[] {
            300,
            0,
            0,
            -2147483648});
			this.DX.Name = "DX";
			this.DX.Size = new System.Drawing.Size(46, 20);
			this.DX.TabIndex = 13;
			// 
			// DY
			// 
			this.DY.Location = new System.Drawing.Point(127, 167);
			this.DY.Maximum = new decimal(new int[] {
            300,
            0,
            0,
            0});
			this.DY.Minimum = new decimal(new int[] {
            300,
            0,
            0,
            -2147483648});
			this.DY.Name = "DY";
			this.DY.Size = new System.Drawing.Size(46, 20);
			this.DY.TabIndex = 14;
			this.DY.Value = new decimal(new int[] {
            30,
            0,
            0,
            0});
			// 
			// DZ
			// 
			this.DZ.Location = new System.Drawing.Point(193, 167);
			this.DZ.Maximum = new decimal(new int[] {
            300,
            0,
            0,
            0});
			this.DZ.Minimum = new decimal(new int[] {
            300,
            0,
            0,
            -2147483648});
			this.DZ.Name = "DZ";
			this.DZ.Size = new System.Drawing.Size(46, 20);
			this.DZ.TabIndex = 15;
			this.DZ.Value = new decimal(new int[] {
            100,
            0,
            0,
            0});
			// 
			// ACZ
			// 
			this.ACZ.Location = new System.Drawing.Point(145, 70);
			this.ACZ.Maximum = new decimal(new int[] {
            300,
            0,
            0,
            0});
			this.ACZ.Minimum = new decimal(new int[] {
            300,
            0,
            0,
            -2147483648});
			this.ACZ.Name = "ACZ";
			this.ACZ.Size = new System.Drawing.Size(46, 20);
			this.ACZ.TabIndex = 9;
			this.ACZ.Value = new decimal(new int[] {
            120,
            0,
            0,
            0});
			// 
			// ACY
			// 
			this.ACY.Location = new System.Drawing.Point(93, 69);
			this.ACY.Maximum = new decimal(new int[] {
            300,
            0,
            0,
            0});
			this.ACY.Minimum = new decimal(new int[] {
            300,
            0,
            0,
            -2147483648});
			this.ACY.Name = "ACY";
			this.ACY.Size = new System.Drawing.Size(46, 20);
			this.ACY.TabIndex = 8;
			this.ACY.Value = new decimal(new int[] {
            100,
            0,
            0,
            -2147483648});
			// 
			// ACX
			// 
			this.ACX.Location = new System.Drawing.Point(41, 70);
			this.ACX.Maximum = new decimal(new int[] {
            300,
            0,
            0,
            0});
			this.ACX.Minimum = new decimal(new int[] {
            300,
            0,
            0,
            -2147483648});
			this.ACX.Name = "ACX";
			this.ACX.Size = new System.Drawing.Size(46, 20);
			this.ACX.TabIndex = 7;
			this.ACX.Value = new decimal(new int[] {
            160,
            0,
            0,
            0});
			// 
			// AC
			// 
			this.AC.AutoSize = true;
			this.AC.Location = new System.Drawing.Point(14, 74);
			this.AC.Name = "AC";
			this.AC.Size = new System.Drawing.Size(21, 13);
			this.AC.TabIndex = 10;
			this.AC.Text = "AC";
			// 
			// label5
			// 
			this.label5.AutoSize = true;
			this.label5.Location = new System.Drawing.Point(14, 34);
			this.label5.Name = "label5";
			this.label5.Size = new System.Drawing.Size(22, 13);
			this.label5.TabIndex = 14;
			this.label5.Text = "OC";
			// 
			// OCZ
			// 
			this.OCZ.Location = new System.Drawing.Point(145, 30);
			this.OCZ.Maximum = new decimal(new int[] {
            300,
            0,
            0,
            0});
			this.OCZ.Minimum = new decimal(new int[] {
            300,
            0,
            0,
            -2147483648});
			this.OCZ.Name = "OCZ";
			this.OCZ.Size = new System.Drawing.Size(46, 20);
			this.OCZ.TabIndex = 13;
			// 
			// OCY
			// 
			this.OCY.Location = new System.Drawing.Point(93, 29);
			this.OCY.Maximum = new decimal(new int[] {
            300,
            0,
            0,
            0});
			this.OCY.Minimum = new decimal(new int[] {
            300,
            0,
            0,
            -2147483648});
			this.OCY.Name = "OCY";
			this.OCY.Size = new System.Drawing.Size(46, 20);
			this.OCY.TabIndex = 12;
			// 
			// OCX
			// 
			this.OCX.Location = new System.Drawing.Point(41, 30);
			this.OCX.Maximum = new decimal(new int[] {
            300,
            0,
            0,
            0});
			this.OCX.Minimum = new decimal(new int[] {
            300,
            0,
            0,
            -2147483648});
			this.OCX.Name = "OCX";
			this.OCX.Size = new System.Drawing.Size(46, 20);
			this.OCX.TabIndex = 11;
			this.OCX.Value = new decimal(new int[] {
            160,
            0,
            0,
            0});
			// 
			// label6
			// 
			this.label6.AutoSize = true;
			this.label6.Location = new System.Drawing.Point(16, 134);
			this.label6.Name = "label6";
			this.label6.Size = new System.Drawing.Size(22, 13);
			this.label6.TabIndex = 15;
			this.label6.Text = "RC";
			// 
			// RC
			// 
			this.RC.Location = new System.Drawing.Point(83, 132);
			this.RC.Maximum = new decimal(new int[] {
            200,
            0,
            0,
            0});
			this.RC.Name = "RC";
			this.RC.Size = new System.Drawing.Size(46, 20);
			this.RC.TabIndex = 16;
			this.RC.Value = new decimal(new int[] {
            50,
            0,
            0,
            0});
			// 
			// SaveButon
			// 
			this.SaveButon.Location = new System.Drawing.Point(313, 356);
			this.SaveButon.Name = "SaveButon";
			this.SaveButon.Size = new System.Drawing.Size(130, 29);
			this.SaveButon.TabIndex = 2;
			this.SaveButon.Text = "SaveButon";
			this.SaveButon.UseVisualStyleBackColor = true;
			this.SaveButon.Click += new System.EventHandler(this.SaveButon_Click);
			// 
			// CancelButton
			// 
			this.CancelButton.Location = new System.Drawing.Point(99, 356);
			this.CancelButton.Name = "CancelButton";
			this.CancelButton.Size = new System.Drawing.Size(130, 29);
			this.CancelButton.TabIndex = 3;
			this.CancelButton.Text = "CancelButton";
			this.CancelButton.UseVisualStyleBackColor = true;
			this.CancelButton.Click += new System.EventHandler(this.CancelButton_Click);
			// 
			// Options
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(556, 466);
			this.ControlBox = false;
			this.Controls.Add(this.CancelButton);
			this.Controls.Add(this.SaveButon);
			this.Controls.Add(this.Conus);
			this.Controls.Add(this.Cube);
			this.MaximizeBox = false;
			this.MinimizeBox = false;
			this.Name = "Options";
			this.ShowIcon = false;
			this.ShowInTaskbar = false;
			this.Text = "Options";
			this.Cube.ResumeLayout(false);
			this.Cube.PerformLayout();
			this.Conus.ResumeLayout(false);
			this.Conus.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.AX)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.AY)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.AZ)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.BX)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.BY)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.BZ)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.CX)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.CY)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.CZ)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.DX)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.DY)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.DZ)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.ACZ)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.ACY)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.ACX)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.OCZ)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.OCY)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.OCX)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.RC)).EndInit();
			this.ResumeLayout(false);

		}

		#endregion

		private System.Windows.Forms.GroupBox Cube;
		private System.Windows.Forms.NumericUpDown AZ;
		private System.Windows.Forms.NumericUpDown AY;
		private System.Windows.Forms.NumericUpDown AX;
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.GroupBox Conus;
		private System.Windows.Forms.NumericUpDown DZ;
		private System.Windows.Forms.NumericUpDown DY;
		private System.Windows.Forms.NumericUpDown DX;
		private System.Windows.Forms.NumericUpDown CZ;
		private System.Windows.Forms.NumericUpDown CY;
		private System.Windows.Forms.NumericUpDown CX;
		private System.Windows.Forms.NumericUpDown BZ;
		private System.Windows.Forms.NumericUpDown BY;
		private System.Windows.Forms.NumericUpDown BX;
		private System.Windows.Forms.Label label5;
		private System.Windows.Forms.NumericUpDown OCZ;
		private System.Windows.Forms.NumericUpDown OCY;
		private System.Windows.Forms.NumericUpDown OCX;
		private System.Windows.Forms.Label AC;
		private System.Windows.Forms.NumericUpDown ACZ;
		private System.Windows.Forms.NumericUpDown ACY;
		private System.Windows.Forms.NumericUpDown ACX;
		private System.Windows.Forms.NumericUpDown RC;
		private System.Windows.Forms.Label label6;
		private System.Windows.Forms.Button SaveButon;
		private System.Windows.Forms.Button CancelButton;
	}
}