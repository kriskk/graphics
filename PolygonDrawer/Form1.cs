﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace PolygonDrawer
{
	public partial class Form1 : Form
	{
		public Options CubeConusOptions;
		public Bitmap Picture;
		private PolygonOrderer orderer = new PolygonOrderer();

		public Cube cube;
//		= new Cube(new DPoint3d(0, 30, 0),
//		                             new DPoint3d(Math.Sqrt(9999), 30, 1),
//		                             new DPoint3d(0, 130, 0),
//		                             new DPoint3d(-1,30 , Math.Sqrt(9999)));
		public Conus conus;
//		= new Conus(new DPoint3d(160, 0,0),
//										new DPoint3d(160, -100, 120), 50);

		private Polygon[] AllPolygons;

		public Point Space2Monitor(DPoint3d p)
		{
			var oX = panel1.Width/2;
			var oY = panel1.Height/2;

			var xx = (int) ((p.Y - p.X)*Math.Sqrt(3)/2);
			var yy = (int) ((p.X + p.Y)/2 - p.Z);
			return new Point(oX + xx, oY + yy);
		}

		private void DrawPicture(IEnumerable<Polygon> polygons)
		{
			var orderedPolygons = orderer.OrderPolygons(polygons);
			Picture = new Bitmap(panel1.Width, panel1.Height);
			var g = Graphics.FromImage(Picture);
			foreach (var pol in orderedPolygons)
			{
				var monitorPoints = pol.Points.Select(Space2Monitor).ToArray();
				g.FillPolygon(pol.ColorBrush, monitorPoints);
				g.DrawPolygon(pol.BorderPen, monitorPoints);
			}

			panel1.CreateGraphics().DrawImage(Picture, 0, 0);
		}


		public Form1()
		{
			InitializeComponent();
			CubeConusOptions = new Options(this);
			CubeConusOptions.SaveButon_Click(null, null);
		}
		public void PictureStart()
		{
//			var tr = new Triangle(new DPoint3d(150, 0, 0), new DPoint3d(170, 50, 0), new DPoint3d(80, 20, 50)) { Color = Color.Blue };
			AllPolygons = cube.GetAllPolygons()
				//				.Concat(new[]{tr})
				.Concat(conus.GetAllPolygons())
				.ToArray();
			timer1.Start();
		}
		public void PictureStop()
		{
			timer1.Stop();
		}

		private void Form1_Load(object sender, EventArgs e)
		{
			PictureStart();
		}

		private void panel1_Paint(object sender, PaintEventArgs e)
		{

			//			var square = new[]
			//			            	{
			//								new DPoint3d(0,0,100),
			//								new DPoint3d(100,0,100),
			//								new DPoint3d(100,100,100),
			//								new DPoint3d(0,100,100),
			//
			//			            	};
			//			var square5 = new[]
			//			            	{
			//								new DPoint3d(0,0,0),
			//								new DPoint3d(100,0,0),
			//								new DPoint3d(100,100,0),
			//								new DPoint3d(0,100,0),
			//
			//			            	};
			//			var square1 = new[]
			//			            	{
			//								new DPoint3d(100, 0, 100),
			//								new DPoint3d(100, 0,0),
			//								new DPoint3d(100, 100, 0),
			//								new DPoint3d(100,100,100),
			//
			//			            	};
			//			var square4 = new[]
			//			            	{
			//								new DPoint3d(0, 0, 100),
			//								new DPoint3d(0, 0,0),
			//								new DPoint3d(0, 100, 0),
			//								new DPoint3d(0,100,100),
			//
			//			            	};
			//			var square2 = new[]
			//			            	{
			//								new DPoint3d(0,100,0),
			//								new DPoint3d(100,100,0),
			//								new DPoint3d(100,100,100),
			//								new DPoint3d(0,100,100),
			//
			//			            	};
			//			var square3 = new[]
			//			            	{
			//								new DPoint3d(0,0,0),
			//								new DPoint3d(100,0,0),
			//								new DPoint3d(100,0,100),
			//								new DPoint3d(0,0,100),
			//
			//			            	};
			DrawPicture(AllPolygons);
		}

		private void timer1_Tick(object sender, EventArgs e)
		{
			double alphaXY = 0.02;
//			orderer.RotateXY(alphaXY);
			foreach (var p in AllPolygons)
			{
				p.RotateXY(alphaXY);
			}
			panel1.Refresh();
		}

		private void optionsToolStripMenuItem_Click(object sender, EventArgs e)
		{
			PictureStop();
			CubeConusOptions.Show();
		}

		
	}
}