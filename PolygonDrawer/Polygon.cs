﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using PolygonDrawer;

namespace PolygonDrawer
{
	public abstract class Polygon
	{
		public readonly DPoint3d[] Points;
		public Color Color = Color.Aqua;
		public Brush ColorBrush{get{return new SolidBrush(Color);}}
		public Color ColorBorder = Color.Black;
		public Pen BorderPen { get { return new Pen(ColorBorder); }}

		protected Polygon(DPoint3d[] points)
		{
			Points = points;
		}

		public virtual DPoint3d GetCenter()
		{
			var center = new DPoint3d((double) 0, 0, 0);
			center = Points.Aggregate(center, (current, t) => current + t);
			center *= 1.0 / Points.Length;
			return center;
		}

		public abstract IEnumerable<Polygon> GetAllPolygons();

		public void Move(DPoint3d p)
		{
			for (int i = 0; i < Points.Length; i++)
			{
				Points[i] += p;
			}
		}

		public void Rotate(double alphaXY, double bettaYZ, double gammaXZ)
		{
			for (int i = 0; i < Points.Length; i++)
			{
				Points[i] = Points[i].Rotate(alphaXY, bettaYZ, gammaXZ);
			}
		}

		public void RotateXY(double alpha)
		{
			for (int i = 0; i < Points.Length; i++)
			{
				Points[i] = Points[i].RotateXY(alpha);
			}
		}
		public void RotateYZ(double alpha)
		{
			for (int i = 0; i < Points.Length; i++)
			{
				Points[i] = Points[i].RotateYZ(alpha);
			}
		}
		public void RotateXZ(double alpha)
		{
			for (int i = 0; i < Points.Length; i++)
			{
				Points[i] = Points[i].RotateXZ(alpha);
			}
		}
	}
}
