﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;

namespace PolygonDrawer
{
	public class Conus
	{
		private double R, h;
		public DPoint3d O, A;
		public Color Color = Color.Coral;

		private double alphaXY, bettaYZ, gammaXZ;

		public Conus(DPoint3d o, DPoint3d a, double r)
		{
			O = o;
			A = a;
			R = r;
			var centralVector = (A - O);
			h = centralVector.Distance;
			centralVector.GetAngles(out alphaXY,out bettaYZ,out gammaXZ);
		}
		/// <summary>
		///  changes Polygon p
		/// </summary>
		private Polygon ToRightPlace(Polygon p)
		{
			p.Rotate(alphaXY,bettaYZ,gammaXZ );
			p.Move(O);
			return p;
		}

		public IEnumerable<Polygon> GetAllPolygons()
		{
			const int n = 20;
			var twoPi = Math.PI*2;
			var step = twoPi/n;
			var ao = new DPoint3d(0, 0, h);
			var oo = new DPoint3d((double) 0, 0, 0);
			for (int i = 0; i < n; i++)
			{
				var bo = new DPoint3d(R*Math.Cos(step*i), R*Math.Sin(step*i), 0);
				var co = new DPoint3d(R*Math.Cos(step*(i+1)), R*Math.Sin(step*(i+1)), 0);
				var tr1 = new Triangle(bo, co, oo) {Color = Color};
				var tr2 = new Triangle(bo, co, ao) {Color = Color};
				yield return ToRightPlace(tr1);
				yield return ToRightPlace(tr2);
			}
		}
	}
}
