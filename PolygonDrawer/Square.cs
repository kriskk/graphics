﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PolygonDrawer
{
	class Square : Polygon
	{
		// A     B
		//  -----
		//  |   |
		//  -----
		// D     C
		public Square(DPoint3d[] points) : base(points)
		{
			if (points.Length != 4)
				throw new ArgumentException("There must be 4 points");
		}
		public Square(DPoint3d a, DPoint3d b, DPoint3d c, DPoint3d d)
			:this(new []{a,b,c,d})
		{

		}

		public override IEnumerable<Polygon> GetAllPolygons()
		{
			yield return this;
		}
	}

	
}
