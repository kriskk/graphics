﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PolygonDrawer
{
	class PolygonOrderer
	{
		private DPoint3d vector;

		public PolygonOrderer()
		{
			vector = new DPoint3d((double) 1,1,1);
		}

		public void Rotate(double alphaXY, double bettaYZ, double gammaXZ)
		{
			vector = vector.Rotate(alphaXY, bettaYZ, gammaXZ);
		}
		public void RotateXY(double alphaXY)
		{
			vector = vector.RotateXY(alphaXY);
		}


		public IEnumerable<Polygon> OrderPolygons(IEnumerable<Polygon> polygons )
		{
			return polygons.OrderBy(p =>
			                        	{
			                        		var c = p.GetCenter();
			                        		var xp = c.X*vector.X;
			                        		var yp = c.Y*vector.Y;
			                        		var zp = c.Z*vector.Z;
			                        		return xp + yp + zp;
			                        	});
		}
	}
}
