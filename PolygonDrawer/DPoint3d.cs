﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;

namespace PolygonDrawer
{
	public class DPoint3d
	{
		public double X, Y, Z;

		public double Distance
		{
			get { return Math.Sqrt(X*X + Y*Y + Z*Z); }
		}

		public DPoint3d(double x, double y, double z)
		{
			X = x;
			Y = y;
			Z = z;
		}
		public DPoint3d(decimal x, decimal y, decimal z)
			:this((double)x, (double)y, (double)z)
		{
			
		}

		public static DPoint3d operator +(DPoint3d p, DPoint3d p2)
		{
			return new DPoint3d(p.X + p2.X, p.Y + p2.Y, p.Z + p2.Z);
		}
		public static DPoint3d operator -(DPoint3d p, DPoint3d p2)
		{
			return new DPoint3d(p.X - p2.X, p.Y - p2.Y, p.Z - p2.Z);
		}

		public static DPoint3d operator *(DPoint3d p, double s)
		{
			return new DPoint3d(p.X*s, p.Y*s, p.Z*s);
		}
		public void GetAngles(out double alphaXY, out double bettaYZ , out double gammaXZ)
		{
			alphaXY = Math.Atan2(Y, X);
			bettaYZ = Math.Atan2(Z, Y) - Math.PI/2;
			gammaXZ = Math.Atan2(X, Z);
		}



		public DPoint3d Rotate(double alphaXY, double bettaYZ , double gammaXZ)
		{
			return RotateXY(alphaXY).RotateYZ(bettaYZ).RotateXZ(gammaXZ);
		}

		// Поворот по осям XY
		public DPoint3d RotateXY(double alpha)
		{
			var tY = Y*Math.Cos(alpha) - X*Math.Sin(alpha);
			var tX = Y*Math.Sin(alpha) + X*Math.Cos(alpha);
			return new DPoint3d(tX, tY, Z);
		}

		// Поворот по осям YZ
		public DPoint3d RotateYZ(double alpha)
		{

			var tZ = Z*Math.Cos(alpha) - Y*Math.Sin(alpha);
			var tY = Z*Math.Sin(alpha) + Y*Math.Cos(alpha);
			return new DPoint3d(X, tY, tZ);
		}

		// Поворот по осям XZ
		public DPoint3d RotateXZ(double alpha)
		{

			var tZ = Z*Math.Cos(alpha) - X*Math.Sin(alpha);
			var tX = Z*Math.Sin(alpha) + X*Math.Cos(alpha);
			return new DPoint3d(tX, Y, tZ);
		}


		public override string ToString()
		{
			return string.Format("({0},{1},{2})", X, Y, Z);
		}
	}

	[TestFixture]
	internal class DPoint3dTester
	{
		[Test]
		public void AnglesTest()
		{
			var p = new DPoint3d((double) 1, 1, 0);
			double a, b, g;
			p.GetAngles(out a,out b,out g);
		} 
	}
}
