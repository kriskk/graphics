﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace PolygonDrawer
{
	public partial class Options : Form
	{
		private Form1 form;
		public Options(Form1 form1)
		{
			InitializeComponent();
			form = form1;
		}

		public void SaveButon_Click(object sender, EventArgs e)
		{
			try
			{
				var A = new DPoint3d(AX.Value, AY.Value, AZ.Value);
				var B = new DPoint3d(BX.Value, BY.Value, BZ.Value);
				var C = new DPoint3d(CX.Value, CY.Value, CZ.Value);
				var D = new DPoint3d(DX.Value, DY.Value, DZ.Value);
				var AC = new DPoint3d(ACX.Value, ACY.Value, ACZ.Value);
				var OC = new DPoint3d(OCX.Value, OCY.Value, OCZ.Value);
				var R = (double) RC.Value;

				var cube = new Cube(A, B, C, D);
				var conus = new Conus(OC, AC, R);

				form.cube = cube;
				form.conus = conus;

				form.PictureStart();
				this.Hide();
			}
			catch (Exception exception)
			{
				MessageBox.Show(exception.Message);
			}
		}

		private void CancelButton_Click(object sender, EventArgs e)
		{
			this.Hide();
		}
	}
}
