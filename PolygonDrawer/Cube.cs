﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using PolygonIntersections;

namespace PolygonDrawer
{
	public class Cube
	{
		//
		private DPoint3d A { get; set; }
		private DPoint3d B { get; set; }
		private DPoint3d C { get; set; }
		private DPoint3d D { get; set; }

		public Cube(DPoint3d a, DPoint3d b, DPoint3d c, DPoint3d d)
		{
			A = a;
			B = b;
			C = c;
			D = d;
			Assert.That(Utilits.AreApproxEqual((B - A).Distance, (C - A).Distance), "Wrong Cube parameters");
			Assert.That(Utilits.AreApproxEqual((C - A).Distance, (D - A).Distance), "Wrong Cube parameters");
			Assert.That(Utilits.AreApproxEqual((D - A).Distance, (B - A).Distance), "Wrong Cube parameters");
		}
		public IEnumerable<Polygon> GetAllPolygons()
		{
			var pACB = B + C - A;
			var pACD = C + D - A;
			var pABD = B + D - A;
			var Q = B + C + D - (A*2);
			yield return new Square(A, B, pACB, C);
			yield return new Square(D,pABD,Q,pACD);
			yield return new Square(A,B,pABD,D);
			yield return new Square(B, pACB, Q,pABD);
			yield return new Square(pACB,C,pACD,Q);
			yield return new Square(C,A,D,pACD);
		}
	}

	[TestFixture]
	internal class CubeTester
	{
		[Test]
		public void CubeTest()
		{
			var cube = new Cube(new DPoint3d((double) 1, 1, 1), new DPoint3d((double) 15, 1, 1), new DPoint3d((double) 1, 15, 1), new DPoint3d((double) 1, 1, 16));
			foreach (var pol in cube.GetAllPolygons())
			{
				var q = pol;
			}
		}
	}
}
